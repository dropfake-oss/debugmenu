# Debug Menu

To "see" the nested containers within the DebugMenuContainer.uxml within Unity Editor - remove the "hidden" from the class in the following:

```
<ui:VisualElement name="container" class="bg-gray-300 h-full m-32 items-center hidden">
```

## Integration

To show and hide this debug menu - use the following in your code:

```
DebugMenu.DebugMenuRoot.Instance.ToggleDebugMenu();
```

Alternatively - if you want to get a return value indicating whether the menu is currently visible or not - you can use the following property:

```
var debugMenuVisible = DebugMenu.DebugMenuRoot.Instance.Show;
```

## Updates

### Release 1.0.17

-   Removed UXML document dependency from DebugMenuRoot.
-   All controls are now available to preview in debug menu.
-   Added a Field in Debug Menu to change text color in debug menu.

## Contribution

Just create a MR and it will be reviewed in a timely fashion with feedback and/or accepted.

:sparkles:
