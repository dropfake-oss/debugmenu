
using DebugMenu;
using System;
using System.Collections.Generic;
using System.Linq;
using UniRedux;
using UniRedux.Entity;
using UniRx;

namespace DebugMenu
{
	public class StandardEntityConnectorDebugMenu<TRootState, TEntity> where TRootState : class, new() where TEntity : class
	{
		private Subject<IReadOnlyList<TEntity>> OnReload = new Subject<IReadOnlyList<TEntity>>();

		public StandardEntityConnectorDebugMenu(
			IDebugMenuPanel parent,
			string name,
			ISelectableReduxStore<TRootState> store,
			StandardEntityConnector<TRootState, TEntity> connector,
			params (string, Action<TEntity>)[] perEntityOps) : this(parent, name, store, connector, null, false, false, perEntityOps)
		{
		}

		public StandardEntityConnectorDebugMenu(
			IDebugMenuPanel parent,
			string name,
			ISelectableReduxStore<TRootState> store,
			StandardEntityConnector<TRootState, TEntity> connector,
			Func<IEnumerable<TEntity>?>? getEntitiesForReload = null,
			params (string, Action<TEntity>)[] perEntityOps) : this(parent, name, store, connector, getEntitiesForReload, true, true, perEntityOps)
		{
		}

		public StandardEntityConnectorDebugMenu(
			IDebugMenuPanel parent,
			string name,
			ISelectableReduxStore<TRootState> store,
			StandardEntityConnector<TRootState, TEntity> connector,
			Func<IEnumerable<TEntity>?>? getEntitiesForReload,
			bool includeClear,
			bool includeDelete,
			params (string, Action<TEntity>)[] perEntityOps)
		{
			var panel = parent.AddPanel(name);

			var ops = new List<(string, Action<TEntity>)>(perEntityOps.Length + 1);
			ops.AddRange(perEntityOps);

			if (includeDelete == true)
			{
				Action<TEntity> onDelete = (entity) => store.Dispatch(new StandardEntityConnector<TRootState, TEntity>.RemoveOne(entity));
				var deleteOp = ("Delete", onDelete);
				ops.Add(deleteOp);
			}

			var plural = typeof(TEntity).Name + "(s)";
			panel.AddLabelledList(plural, store.Select(connector.SelectEntities), ops.ToArray());

			if (getEntitiesForReload != null)
			{
				panel.AddButton("Reload " + plural, () =>
				{
					var entities = getEntitiesForReload();
					if (entities != null)
					{
						this.OnReload.OnNext(entities.ToList());
					}
				});
			}

			if (includeClear == true)
			{
				panel.AddButton("Clear All " + plural, () =>
				{
					store.Dispatch(new StandardEntityConnector<TRootState, TEntity>.RemoveAll());
				});
			}

			connector.RegisterForAddAll(store, this.OnReload);
		}
	}

	public class StandardEntityConnectorDebugMenuWithChanges<TRootState, TEntity> where TRootState : class, new() where TEntity : class, IWithChanges<TEntity>
	{
		private Subject<IReadOnlyList<TEntity>> OnReload = new Subject<IReadOnlyList<TEntity>>();

		public StandardEntityConnectorDebugMenuWithChanges(
			IDebugMenuPanel parent,
			string name,
			ISelectableReduxStore<TRootState> store,
			StandardEntityConnector<TRootState, TEntity> connector,
			params (string, Action<TEntity>)[] perEntityOps) : this(parent, name, store, connector, null, false, false, perEntityOps)
		{
		}

		public StandardEntityConnectorDebugMenuWithChanges(
			IDebugMenuPanel parent,
			string name,
			ISelectableReduxStore<TRootState> store,
			StandardEntityConnector<TRootState, TEntity> connector,
			Func<IEnumerable<TEntity>?>? getEntitiesForReload = null,
			params (string, Action<TEntity>)[] perEntityOps) : this(parent, name, store, connector, getEntitiesForReload, true, true, perEntityOps)
		{
		}

		public StandardEntityConnectorDebugMenuWithChanges(
			IDebugMenuPanel parent,
			string name,
			ISelectableReduxStore<TRootState> store,
			StandardEntityConnector<TRootState, TEntity> connector,
			Func<IEnumerable<TEntity>?>? getEntitiesForReload,
			bool includeClear,
			bool includeDelete,
			params (string, Action<TEntity>)[] perEntityOps)
		{
			var panel = parent.AddPanel(name);

			var ops = new List<(string, Action<TEntity>)>(perEntityOps.Length + 1);
			ops.AddRange(perEntityOps);

			if (includeDelete == true)
			{
				Action<TEntity> onDelete = (entity) => store.Dispatch(new StandardEntityConnector<TRootState, TEntity>.RemoveOne(entity));
				var deleteOp = ("Delete", onDelete);
				ops.Add(deleteOp);
			}

			var plural = typeof(TEntity).Name + "(s)";
			panel.AddLabelledList(plural, store.Select(connector.SelectEntities), entry => entry.Changed.Select((e) => e.ToString()), ops.ToArray());

			if (getEntitiesForReload != null)
			{
				panel.AddButton("Reload " + plural, () =>
				{
					var entities = getEntitiesForReload();
					if (entities != null)
					{
						this.OnReload.OnNext(entities.ToList());
					}
				});
			}

			if (includeClear == true)
			{
				panel.AddButton("Clear All " + plural, () =>
				{
					store.Dispatch(new StandardEntityConnector<TRootState, TEntity>.RemoveAll());
				});
			}

			connector.RegisterForAddAll(store, this.OnReload);
		}
	}
}
