using UniRx;
using UnityEngine;
using System.Collections.Generic;

namespace DebugMenu
{
	public class ExampleDebugMenu
	{
		private float TimeSinceLastSwitch = 0;
		private BehaviorSubject<bool> OneSecondSwitcher = new BehaviorSubject<bool>(false);

		private ExampleDebugMenu(IDebugMenuPanel parent)
		{
			var panel = parent.AddPanel("Example");
			panel.AddLabel("Label");

			panel.AddLabelledValue("LabelledValue - Switch Every Second:", this.OneSecondSwitcher, new AddLabelledValueOptions { Compact = true });

			panel.AddSlider("Slider", 0.0f, 10.0f, 6.4f, (v) =>
			{
				Debug.Log("Slider Value: " + v);
			});

			panel.AddToggle("Toggle", true, (v) =>
			{
				Debug.Log("Toggle Value: " + v);
			});

			List<string> dropDownValues = new List<string>() { "foo", "bar" };
			panel.AddDropDown("Test DropDown", dropDownValues, (selectedValue) =>
			{
				Debug.Log("selectedValue: " + selectedValue);
			});

			var update = new Subject<List<string>>();
			update.Subscribe((values) =>
			{
				Debug.Log("values.Count: " + values.Count);
			});
			panel.AddLabelledList("test", update);
			var list = new List<string>() { "test1", "foo" };
			update.OnNext(list);

			panel.AddButton("click me", () =>
			{
				Debug.Log("I was clicked!");
			});

			panel.AddRadioButtonGroup("radio buttons", new List<string>() { "foo", "bar" }, 0, (value) =>
			{
				Debug.Log("value: " + value);
			});
		}

		public void Update()
		{
			this.TimeSinceLastSwitch += Time.deltaTime;
			if (this.TimeSinceLastSwitch > 1)
			{
				var current = this.OneSecondSwitcher.Value;
				this.OneSecondSwitcher.OnNext(!current);
				this.TimeSinceLastSwitch = 0;
			}
		}

		public static ExampleDebugMenu AddExampleMenu(IDebugMenuPanel parent)
		{
			var menu = new ExampleDebugMenu(parent);
			return menu;
		}
	}
}
