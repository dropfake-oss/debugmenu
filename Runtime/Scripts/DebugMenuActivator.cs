using UI.Core;
using UnityEngine;
using UnityEngine.InputSystem;

namespace DebugMenu
{
	public class DebugMenuActivator : MonoBehaviourWithSubscriptions
	{
		private void Update()
		{
			var debugMenu = DebugMenu.DebugMenuRoot.Instance;

			if (debugMenu == null)
			{
				return;
			}

			var toggleDebugMenu = false;
#if UNITY_IOS || UNITY_ANDROID || UNITY_EDITOR // include editor for Unity Remote
			if (Touchscreen.current != null && Touchscreen.current.touches.Count == 4)
			{
				foreach (var touch in Touchscreen.current.touches)
				{
					if (touch.phase.ReadValue() == UnityEngine.InputSystem.TouchPhase.Began)
					{
						toggleDebugMenu = true;
						break;
					}
				}
			}
#endif

			if (toggleDebugMenu)
			{
				debugMenu.ToggleDebugMenu();
			}
		}
	}
}
