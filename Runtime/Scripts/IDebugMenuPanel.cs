using System;
using UI.Core;
using UnityEngine.UIElements;
using System.Collections.Generic;

namespace DebugMenu
{
	public class AddToOptions
	{
		public int Priority { get; set; } = 0;
	}

	public class AddLabelledValueOptions : AddToOptions
	{
		public const bool DefaultCompact = false;

		public bool Compact { get; set; } = DefaultCompact;
	}

	public interface IDebugMenuPanel
	{
		IDebugMenuPanel AddPanel(string labelText, AddToOptions options = null);
		VisualElement AddButton(string name, Action method);
		VisualElement AddLabel(string label, AddToOptions options = null);
		VisualElementWithSubscriptions AddLabelledValue<T>(string label, IObservable<T> source, AddLabelledValueOptions options = null);
		VisualElementWithSubscriptions AddSlider(string label, float min, float max, float current, Action<float> onChanged, AddToOptions options = null);
		VisualElement AddToggle(string label, bool current, Action<bool> onChanged, AddToOptions options = null);
		VisualElement AddDropDown(string label, List<string> choices, Action<string> onSelect);
		(VisualElementWithSubscriptions, IDisposable) AddLabelledList<T>(string label, IObservable<IReadOnlyCollection<T>> source, params (string, Action<T>)[] ops);
		(VisualElementWithSubscriptions, IDisposable) AddLabelledList<T>(string label, IObservable<IReadOnlyCollection<T>> source, Func<T, IObservable<string>> toValue, params (string, Action<T>)[] ops);
		VisualElement AddRadioButtonGroup(string label, IEnumerable<string> choices, int selectedIndex, Action<string> selectedChoice);
	}
}
