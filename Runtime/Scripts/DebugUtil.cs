using System;
using System.Collections.Generic;
using System.ComponentModel;
using UI.Core;
using UniRx;
using UnityEngine;
using UnityEngine.UIElements;

namespace DebugMenu
{
	static class DebugUtil
	{
		public static UnityEngine.Color DefaultBgColor = UnityEngine.Color.white;

		public static void SetTransparency(VisualElement visElement, float alpha)
		{
			// TPC: not sure the right way to set the alpha on the debug menu - from this file:
			// unity-prototype2/Packages/com.dropfake.debugmenu/Runtime/Uxml/DebugMenuContainer.uxml
			// the color is defined here:
			// <ui:VisualElement name="container" class="bg-gray-300 h-full m-32 items-center hidden">
			// but adding "bg-opacity-50" to class doesn't seem to affect transparency/alpha

			// TPC: the following does give us the transparency/alpha we want - but the font of the Foldout isn't as legible when we use the following
			var styleColor = visElement.style.backgroundColor;
			var color = styleColor.value;
			color.a = alpha;
			visElement.style.backgroundColor = color;
		}

		private static bool walkChildren<T>(VisualElement visElement, Func<T, bool> callback) where T : class
		{
			if (callback == null)
			{
				throw new System.Exception("no callback");
			}
			// traverse VisualElement hierarchy
			var visELementhildren = visElement.Children();
			foreach (var child in visELementhildren)
			{
				if (child is T)
				{
					bool continueTraversal = callback(child as T);
					if (!continueTraversal)
					{
						return false;
					}
				}

				var continueWalk = walkChildren(child, callback);
				if (!continueWalk)
				{
					return false;
				}
			}
			return true;
		}

		public static void WalkChildren<T>(VisualElement visElement, Func<T, bool> callback) where T : class
		{
			walkChildren(visElement, callback);
		}

		private static bool walkHierarchy<T>(VisualElement visElement, Func<T, bool> callback) where T : class
		{
			if (callback == null)
			{
				return false;
			}
			// traverse VisualElement hierarchy
			var hierarchy = visElement.hierarchy;
			var visELementhildren = hierarchy.Children();
			foreach (var child in visELementhildren)
			{
				if (child is T)
				{
					bool continueTraversal = callback(child as T);
					if (!continueTraversal)
					{
						return false;
					}
				}

				var continueWalk = walkHierarchy(child, callback);
				if (!continueWalk)
				{
					return false;
				}
			}
			return true;
		}

		public static void WalkHierarchy<T>(VisualElement visElement, Func<T, bool> callback) where T : class
		{
			walkHierarchy(visElement, callback);
		}

		private static VisualElementWithSubscriptions GetNewContainer(string label, bool justifyBetween = true)
		{
			var container = new VisualElementWithSubscriptions() { name = label + "Container" };
			container.AddToClassList("flex-row");
			if (justifyBetween)
			{
				container.AddToClassList("justify-between");
			}
			return container;
		}

		public static VisualElement AddLabel(VisualElement container, string label, AddToOptions options = null)
		{
			var priority = (options != null) ? options.Priority : 0;
			var lv = GetNewContainer(label);

			var l = new Label() { name = "label" };
			l.text = label;

			lv.Add(l);

			container.Add(lv);

			if (priority < 0)
			{
				container.Sort((a, b) =>
				{
					if (a == lv)
					{
						return -1;
					}

					return 0;
				});
			}

			return lv;
		}

		private static void AddAndSortContainer(VisualElement container, VisualElement ve, AddToOptions options = null)
		{
			var priority = (options != null) ? options.Priority : 0;

			container.Add(ve);
			if (priority < 0)
			{
				container.Sort((a, b) =>
				{
					if (a == ve)
					{
						return -1;
					}

					return 0;
				});
			}
		}

		public static VisualElementWithSubscriptions AddLabelledValue<T>(VisualElement container, string label, IObservable<T> source, AddLabelledValueOptions options = null)
		{
			var compact = (options != null) ? options.Compact : AddLabelledValueOptions.DefaultCompact;

			var lablledValueContainer = new VisualElementWithSubscriptions();
			lablledValueContainer.AddToClassList("flex-row");
			lablledValueContainer.AddToClassList("flex-grow");
			if (compact == false)
			{
				lablledValueContainer.AddToClassList("justify-between");
			}

			var l = new Label();
			l.text = label;

			var v = new Label();
			var sub = source.ObserveOnMainThread().Subscribe(current =>
			{
				v.text = (current != null) ? current.ToString() : string.Empty;
			});
			lablledValueContainer.AddSubscription(sub);

			lablledValueContainer.Add(l);
			lablledValueContainer.Add(v);

			container.Add(lablledValueContainer);
			return lablledValueContainer;
		}


		public static (VisualElementWithSubscriptions, IDisposable) AddLabelledList<T>(VisualElement container, string label, IObservable<IReadOnlyCollection<T>> source, params (string, Action<T>)[] ops)
		{
			return AddLabelledList(container, label, source, entry => Observable.Return(entry.ToString()), ops);
		}

		public static (VisualElementWithSubscriptions, IDisposable) AddLabelledList<T>(VisualElement container, string label, IObservable<IReadOnlyCollection<T>> source, Func<T, IObservable<string>> toValue, params (string, Action<T>)[] ops)
		{
			var ll = GetNewContainer(label, false);

			var l = new Label() { name = label + "label" };
			l.text = label;

			var list = new Foldout();
			list.Clear();

			ll.Add(l);
			ll.Add(list);

			var sub = source.ObserveOnMainThread().Subscribe(current =>
			{
				list.Clear();
				list.text = current.Count.ToString();

				foreach (var entry in current)
				{
					VisualElement lc = list;
					if (ops.Length > 0)
					{
						lc = new VisualElementWithSubscriptions();
						lc.AddToClassList("flex-row");
						lc.AddToClassList("flex-grow");
						list.Add(lc);
					}

					var entryLabel = new Label();
					var entrySource = toValue(entry);
					var sub = entrySource.ObserveOnMainThread().Subscribe(current =>
					{
						entryLabel.text = current.ToString();
					});
					ll.AddSubscription(sub);
					lc.Add(entryLabel);

					if (ops.Length > 0)
					{
						var spacer = new VisualElementWithSubscriptions();
						spacer.AddToClassList("flex-grow");
						lc.Add(spacer);

						foreach (var (label, op) in ops)
						{
							AddButton(lc, label, () => op(entry));
						}
					}
				}
			});

			container.Add(ll);
			return (ll, sub);
		}

		public static VisualElement AddButton(VisualElement visElementParent, string name, Action method)
		{
			var button = new Button() { text = name };
			button.name = name;
			button.clicked += method;

			button.style.width = StyleKeyword.Auto;
			button.style.height = 30;

			visElementParent.Add(button);

			return button;
		}

		public static VisualElement AddToggle(VisualElement container, string label, Action<bool> checkedCallback, bool selected = false, AddToOptions options = null)
		{
			var lv = GetNewContainer(label);

			var toggle = new Toggle() { name = label + "Container" };
			toggle.AddToClassList("text-middle-left");
			toggle.style.alignItems = Align.Center; //Todo: Update DFTailwind repo with some classes to support this.


			// TPC: odd the Toggle has a "text" field as well (one is before and the other is after the checkbox)
			//toggle.text = label;
			toggle.label = label;

			toggle.SetValueWithoutNotify(selected);
			// TPC: not sure if we should use the toggle.value = selected instead

			toggle.RegisterValueChangedCallback((ChangeEvent<bool> checkedEvent) =>
			{
				checkedCallback(checkedEvent.newValue);
			});

			lv.Add(toggle);

			container.Add(lv);
			return lv;
		}

		public static VisualElement AddRadioButtonGroup(VisualElement container, string label, IEnumerable<string> choices, int selectedIndex, Action<string> selectedCallback)
		{
			var lv = GetNewContainer(label);

			var radioButtonGroup = new RadioButtonGroup() { name = label };
			radioButtonGroup.AddToClassList("text-middle-center");
			radioButtonGroup.style.alignItems = Align.FlexStart; //Todo: Update DFTailwind repo with some classes to support this.

			// TPC: odd the Toggle has a "text" field as well (one is before and the other is after the checkbox)
			//toggle.text = label;
			radioButtonGroup.label = label;
			radioButtonGroup.choices = choices;

			radioButtonGroup.RegisterValueChangedCallback((ChangeEvent<int> selectedEvent) =>
			{
				UnityEngine.Debug.Log("DebugUtil: AddRadioButtonGroup: " + selectedEvent.newValue);

				var choicesList = choices as IList<string>;
				if (choicesList != null)
				{
					var selectedString = choicesList[selectedEvent.newValue];
					selectedCallback(selectedString);
					return;
				}

				int i = 0;
				foreach (var choice in choices)
				{
					if (i == selectedEvent.newValue)
					{
						selectedCallback(choice);
						return;
					}
					++i;
				}
			});

			radioButtonGroup.SetValueWithoutNotify(selectedIndex);

			lv.Add(radioButtonGroup);
			container.Add(lv);
			return lv;
		}

		// TPC: TODO: once we have compatible versions of Unity and UIToolkit - we need to remove the hack/workaround in this method for getting a reference to the DropdownField from UXML
		// the following is the approach recommended in the Unity Samples (Window -> UI Toolkit -> Samples) - but it throws "Element 'DropdownField' has no registered factory method."
		// From what I've read online it appears to be because "bug with the editor having outdated file templates"
		public static VisualElement AddDropDown(VisualElement container, string label, List<string> choices, Action<string> checkedCallback)
		{
			var lv = GetNewContainer(label);

			DropdownField dropdownField = new DropdownField(label, choices, 0);

			dropdownField.value = choices[0];

			dropdownField.RegisterCallback<ChangeEvent<string>>((evt) =>
			{
				UnityEngine.Debug.Log("DebugUtil: AddDropDown: dropdownField: RegisterCallback: evt.newValue: " + evt.newValue + " evt: " + evt);
				//dropdownField.value = evt.newValue;
				checkedCallback(evt.newValue);
			});

			lv.Add(dropdownField);
			container.Add(dropdownField);

			UnityEngine.Debug.Log("DebugUtil: AddDropDown: added DropdownField to Container");

			return lv;
		}


		public static VisualElement AddTextBox(VisualElement container, string label, string value, Action<string> inputStringCallback, bool compact = false, AddToOptions options = null)
		{
			var priority = (options != null) ? options.Priority : 0;

			var visElementContainer = GetNewContainer(label);
			visElementContainer.style.flexDirection = FlexDirection.Row;

			if (!compact)
			{
				visElementContainer.style.justifyContent = Justify.SpaceBetween;
			}

			var l = new Label() { name = "label" };
			l.text = label;

			var v = new TextField() { name = "value" };

			visElementContainer.Add(l);
			visElementContainer.Add(v);

			/*
			var sub = source.ObserveOnMainThread().Subscribe(current =>
			{
				var v = lv.Q<Label>("value");
				if (v != null)
				{
					v.text = current.ToString();
				}
			});
			*/

			if (inputStringCallback != null)
			{
				v.value = value;
				v.RegisterValueChangedCallback((ChangeEvent<string> textChangedEvt) =>
				{
					UnityEngine.Debug.Log("AddTextBox: text: " + textChangedEvt.newValue);
					inputStringCallback(textChangedEvt.newValue);
				});
			}

			container.Add(visElementContainer);
			if (priority < 0)
			{
				container.Sort((a, b) =>
				{
					if (a == visElementContainer)
					{
						return -1;
					}

					return 0;
				});
			}

			//return (lv, sub);
			return visElementContainer;
		}

		public static VisualElementWithSubscriptions AddSlider(VisualElement container, string label, float min, float max, float current, Action<float> onChanged, AddToOptions options = null)
		{
			var sliderContainer = GetNewContainer(label);

			var value = new Label();
			value.text = current.ToString();
			value.AddToClassList("w-12");

			var slider = new Slider(label, min, max, SliderDirection.Horizontal);
			slider.value = current;
			slider.AddToClassList("flex-grow");

			slider.RegisterCallback<ChangeEvent<float>>((ev) =>
			{
				value.text = ev.newValue.ToString();
				onChanged(ev.newValue);
			});

			//slider.showInputField = true;
			sliderContainer.Add(slider);
			sliderContainer.Add(value);

			container.Add(sliderContainer);
			return sliderContainer;
		}
	}
}
