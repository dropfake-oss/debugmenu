using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;


namespace DebugMenu
{
	public static class DebugMenuTest
	{
		public static void AddTestPanel(DebugMenuRoot debugMenu)
		{
			var debugPanel = debugMenu.AddPanel("Sweet Test");
			// TPC: test
			for (int i = 0; i < 30; ++i)
			{
				debugPanel.AddButton("Subpanel" + i, () =>
				{
					Debug.Log("Subpanel: button");
				});
			}
		}

		public static void AddTestButtons(DebugMenuRoot debugMenu)
		{
			List<VisualElement> rootElements = new List<VisualElement>();

			VisualElement panelContainer = new VisualElement();
			Color color = new Color(70, 70, 70, 0.5f);
			panelContainer.style.backgroundColor = color;

			ScrollView panelScroll = new ScrollView();
			panelContainer.Add(panelScroll);

			debugMenu.AddButton("foobar24", panelScroll, () =>
			{
				Debug.Log("in sub button 24");
				for (int i = 0; i < rootElements.Count; ++i)
				{
					//Debug.Log("enabledInHierarchy: " + rootElements[i].enabledInHierarchy + "enabledSelf: " + rootElements[i].enabledSelf);
					// this works - but the buttons continue to occupy the space
					//rootElements[i].style.visibility = (rootElements[i].style.visibility == Visibility.Hidden) ? Visibility.Visible : Visibility.Hidden;

					var buttonName = "Foobar" + i;
					var outButton = debugMenu.PanelsContainer.Q<Button>(buttonName);
					if (outButton != null)
					{
						debugMenu.PanelsContainer.Remove(rootElements[i]);
					}
				}
			});

			debugMenu.PanelsContainer.Add(panelContainer);

			for (int i = 0; i < 30; ++i)
			{
				var buttonVisElement = debugMenu.AddButton("Foobar" + i, debugMenu.PanelsContainer, () =>
				{
					Debug.Log("DebugMenu: Foobar");
				});
				rootElements.Add(buttonVisElement);
			}

			debugMenu.AddButton("foobar42", debugMenu.PanelsContainer, () =>
			{
				Debug.Log("in sub button");

				for (int i = 0; i < rootElements.Count; ++i)
				{
					//Debug.Log("enabledInHierarchy: " + rootElements[i].enabledInHierarchy + "enabledSelf: " + rootElements[i].enabledSelf);
					bool enabled = rootElements[i].enabledInHierarchy;
					rootElements[i].SetEnabled(!enabled);
				}
			});
		}

	}
}
