//#define DEBUG_DEBUGMENU

using System;
using System.Collections.Generic;
using UI.Core;
using UnityEngine;
using UnityEngine.UIElements;

namespace DebugMenu
{
	public class DebugPanel : VisualElement, IDebugMenuPanel
	{
		private VisualElement _debugPanelVisualElement;
		private Foldout _foldout = default;
		// TPC: Unity (UI Toolkit) doesn't appear to support nested ScrollViews - so ditching that idea

		private VisualElement _contentContainer;

		private Dictionary<string, DebugPanel> _panels = new Dictionary<string, DebugPanel>();

		public DebugPanel(string text)
		{
			//Creating the Panel
			_debugPanelVisualElement = new VisualElement();
			_foldout = new Foldout();
			_foldout.AddToClassList("bg-gray-500");
			_foldout.AddToClassList("text-xs");
			_debugPanelVisualElement.Add(_foldout);
			_contentContainer = _foldout;

			Log("DebugPanel: _foldout.style: {0}", _foldout.style.color);
			_foldout.text = text;
			_foldout.value = false; // TPC: peculiar API - setting value to false is how you collapse the Foldout
			this.Add(_debugPanelVisualElement);

			// TPC: this hack is necessary because Unity's Foldout doesn't expose a way to update the style (font, bg color, etc) on the Foldout's Label
			DebugUtil.WalkHierarchy(_foldout, (Toggle toggle) =>
			{
				toggle.style.backgroundColor = DebugUtil.DefaultBgColor;
				return false;
			});
		}

		public VisualElement AddButton(string name, Action method)
		{
			var button = DebugUtil.AddButton(_contentContainer, name, method);
			return button;
		}

		public VisualElementWithSubscriptions AddLabelledValue<T>(string label, IObservable<T> source, AddLabelledValueOptions options = null)
		{
			return DebugUtil.AddLabelledValue(_contentContainer, label, source, options);
		}

		public (VisualElementWithSubscriptions, IDisposable) AddLabelledList<T>(string label, IObservable<IReadOnlyCollection<T>> source, params (string, Action<T>)[] ops)
		{
			return DebugUtil.AddLabelledList(_contentContainer, label, source, ops);
		}

		public (VisualElementWithSubscriptions, IDisposable) AddLabelledList<T>(string label, IObservable<IReadOnlyCollection<T>> source, Func<T, IObservable<string>> toValue, params (string, Action<T>)[] ops)
		{
			return DebugUtil.AddLabelledList(_contentContainer, label, source, toValue, ops);
		}

		public VisualElement AddToggle(string label, Action<bool> checkedCallback, bool selected = false)
		{
			return DebugUtil.AddToggle(_contentContainer, label, checkedCallback, selected);
		}

		public VisualElement AddToggle(string label, bool current, Action<bool> onChanged, AddToOptions options = null)
		{
			return DebugUtil.AddToggle(_contentContainer, label, onChanged, current, options);
		}

		private DebugPanel AddPanel(VisualElement visElement, string labelText, AddToOptions options = null)
		{
			DebugPanel debugPanel;
			bool foundPanel = _panels.TryGetValue(labelText, out debugPanel);
			if (foundPanel)
			{
				bool removed = _panels.Remove(labelText);
				if (!removed)
				{
					throw new System.Exception("AddPanel: _panels contains id: " + labelText + " but we weren't able to remove it!");
				}

				visElement.Remove(debugPanel);
			}
			debugPanel = new DebugPanel(labelText);
			_panels[labelText] = debugPanel;

			visElement.Add(debugPanel);
			if (visElement.style == null)
			{
				throw new System.Exception("DebugPanel: AddPanel: visElement is null!");
			}
			Log("DebugPanel: AddPanel: visElement.style.width: {0}", visElement.style.width);

			return debugPanel;
		}

		public VisualElement AddRadioButtonGroup(string label, IEnumerable<string> choices, int selectedIndex, Action<string> selectedChoice)
		{
			return DebugUtil.AddRadioButtonGroup(_contentContainer, label, choices, selectedIndex, selectedChoice);
		}

		public VisualElement AddDropDown(string label, List<string> choices, Action<string> onSelect)
		{
			return DebugUtil.AddDropDown(_contentContainer, label, choices, onSelect);
		}

		public VisualElement AddTextBox(string label, string value, Action<string> inputStringCallback, bool compact = false, AddToOptions options = null)
		{
			return DebugUtil.AddTextBox(_contentContainer, label, value, inputStringCallback, compact, options);
		}

		public VisualElement AddLabel(string label, AddToOptions options = null)
		{
			return DebugUtil.AddLabel(_contentContainer, label, options);
		}

		private void Log(string format, params object[] args)
		{
#if DEBUG_DEBUGMENU
			Debug.LogFormat(format, args);
#endif
		}

		public IDebugMenuPanel AddPanel(string labelText, AddToOptions options = null)
		{
			return AddPanel(_contentContainer, labelText, options);
		}

		public VisualElementWithSubscriptions AddSlider(string label, float min, float max, float current, Action<float> onChanged, AddToOptions options = null)
		{
			return DebugUtil.AddSlider(_contentContainer, label, min, max, current, onChanged, options);
		}
	}
}
