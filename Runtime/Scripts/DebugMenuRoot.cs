using System;
using System.Collections.Generic;
using UI.Core;
using UnityEngine;
using UnityEngine.UIElements;
using UniRx;

namespace DebugMenu
{
	[RequireComponent(typeof(UIDocument))]
	public class DebugMenuRoot : UnityEngine.MonoBehaviour, IDebugMenuPanel
	{
		private VisualElement RootContainer { get; set; } = default!;
		public ScrollView PanelsContainer { get; private set; } = default!;

		private bool updateErrorEmitted = false;

		public static BehaviorSubject<DebugMenuRoot?> InstanceSubject = new BehaviorSubject<DebugMenuRoot?>(null);
		public static DebugMenuRoot? Instance { get { return InstanceSubject.Value; } }

		private Dictionary<string, DebugPanel> _panels = new Dictionary<string, DebugPanel>();

		[SerializeField] private Color DebugMenuFontColor = Color.white;
		public VisualElement DebugVisualElement
		{
			get { return this.RootContainer; }
		}

		private void Awake()
		{
			InstanceSubject.OnNext(this);
		}

		void OnEnable()
		{
			var doc = GetComponent<UIDocument>();
			// Get references From uxml
			this.RootContainer = doc.rootVisualElement.Q("container");
			this.PanelsContainer = doc.rootVisualElement.Q<ScrollView>("panels-with-scroll");

			if (this.PanelsContainer == null)
			{
				throw new System.Exception("DebugMenuRoot: OnEnable: unable to instantiate ScrollView from UIDocument!");
			}

			//Remove the setup panel
			this.PanelsContainer.Clear();
		}

		public bool Show
		{
			get
			{
				return this.RootContainer.style.display == DisplayStyle.Flex;
			}
			set
			{
				if ((this.Show == false) && (value == true))
				{
					this.ShowDebugMenu();
				}
				else if ((this.Show == true) && (value == false))
				{
					this.HideDebugMenu();
				}
			}
		}

		public void ToggleDebugMenu()
		{
			this.Show = !this.Show;
		}

		public void ShowDebugMenu()
		{
			if (this.PanelsContainer == null)
			{
				if (!updateErrorEmitted)
				{
					throw new System.Exception("DebugMenuRoot: ToggleDebugMenu: PanelsContainer is null!");
				}
				return;
			}


			this.RootContainer.style.display = DisplayStyle.Flex;
			this.RootContainer.RemoveFromClassList("hidden");
			this.RootContainer.BringToFront();

			SetDebugMenuTransparent(DebugMenuFontColor);
		}

		public void HideDebugMenu()
		{
			this.RootContainer.style.display = DisplayStyle.None;
		}

		private void SetDebugMenuTransparent(Color textColor)
		{
			// TPC: we wanted to set transparency using uxml/uss - exp: "bg-opacity-50" - but Unity has a limitation that doesn't allow us to inject "var" into function - exp the following is not supported in Unity .uss files:
			// .bg-black {
			//	background - color: rgba(0, 0, 0, var(--tw - bg - opacity, 1));
			// }
			DebugUtil.SetTransparency(this.RootContainer, 0.5f);

			// TPC: unfortunately if we're going to programmatically set the DebugMenu transparent - it also means we have to programmatically set the various Label Fonts - so they are legible
			DebugUtil.WalkChildren(this.RootContainer, (Label label) =>
			{
				if ((label != null) && (label.style != null))
				{
					label.style.color = textColor;
				}
				return true;
			});
		}


		public void AddToggle(string label, Action<bool> checkedCallback, bool selected = false)
		{
			DebugUtil.AddToggle(PanelsContainer, label, checkedCallback, selected);
		}

		public VisualElement AddToggle(string label, bool current, Action<bool> onChanged, AddToOptions options = null)
		{
			return DebugUtil.AddToggle(PanelsContainer, label, onChanged, current, options);
		}

		private DebugPanel AddPanel(VisualElement visElement, string labelText, AddToOptions options = null)
		{
			var priority = (options != null) ? options.Priority : 0;

			DebugPanel debugPanel;
			bool foundPanel = _panels.TryGetValue(labelText, out debugPanel);
			if (foundPanel)
			{
				bool removed = _panels.Remove(labelText);
				if (!removed)
				{
					throw new System.Exception("AddPanel: _panels contains id: " + labelText + " but we weren't able to remove it!");
				}

				visElement.Remove(debugPanel);
			}
			debugPanel = new DebugPanel(labelText);
			_panels[labelText] = debugPanel;

			visElement.Add(debugPanel);

			//If the priority is <0 then we need to add this at the front
			if (priority < 0)
			{
				visElement.Sort((a, b) =>
				{
					if (a == debugPanel)
					{
						return -1;
					}

					return 0;
				});
			}

			return debugPanel;
		}

		public DebugPanel AddPanel(string labelText, AddToOptions options = null)
		{
			return AddPanel(PanelsContainer, labelText, options);
		}

		public VisualElement AddButton(string name, VisualElement visElement, Action method)
		{
			var button = DebugUtil.AddButton(visElement, name, method);
			return button;
		}

		public VisualElement AddButton(string name, Action method)
		{
			var button = DebugUtil.AddButton(PanelsContainer, name, method);
			return button;
		}

		public VisualElementWithSubscriptions AddLabelledValue<T>(string label, IObservable<T> source, AddLabelledValueOptions options = null)
		{
			return DebugUtil.AddLabelledValue(PanelsContainer, label, source, options);
		}

		public (VisualElementWithSubscriptions, IDisposable) AddLabelledList<T>(string label, IObservable<IReadOnlyCollection<T>> source, params (string, Action<T>)[] ops)
		{
			return DebugUtil.AddLabelledList(PanelsContainer, label, source, ops);
		}

		public (VisualElementWithSubscriptions, IDisposable) AddLabelledList<T>(string label, IObservable<IReadOnlyCollection<T>> source, Func<T, IObservable<string>> toValue, params (string, Action<T>)[] ops)
		{
			return DebugUtil.AddLabelledList(PanelsContainer, label, source, toValue, ops);
		}

		public VisualElement AddRadioButtonGroup(string label, IEnumerable<string> choices, int selectedIndex, Action<string> selectedChoice)
		{
			return DebugUtil.AddRadioButtonGroup(PanelsContainer, label, choices, selectedIndex, selectedChoice);
		}

		public VisualElement AddDropDown(string label, List<string> choices, Action<string> onSelect)
		{
			return DebugUtil.AddDropDown(PanelsContainer, label, choices, onSelect);
		}

		public VisualElement AddTextBox(string label, string value, Action<string> inputStringCallback, bool compact = false, AddToOptions options = null)
		{
			return DebugUtil.AddTextBox(PanelsContainer, label, value, inputStringCallback, compact, options);
		}

		public VisualElement AddLabel(string label, AddToOptions options = null)
		{
			return DebugUtil.AddLabel(PanelsContainer, label, options);
		}

		IDebugMenuPanel IDebugMenuPanel.AddPanel(string labelText, AddToOptions options)
		{
			var panel = this.AddPanel(labelText, options);
			return panel as IDebugMenuPanel;
		}

		public VisualElementWithSubscriptions AddSlider(string label, float min, float max, float current, Action<float> onChanged, AddToOptions options = null)
		{
			return DebugUtil.AddSlider(PanelsContainer, label, min, max, current, onChanged, options);
		}
	}
}
